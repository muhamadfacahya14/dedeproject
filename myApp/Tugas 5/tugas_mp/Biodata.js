import React, {Component} from 'react';
import { StyleSheet, Text, View, Image, Button } from 'react-native';

export default class Biodata extends Component{
    render(){
        return(
            <View style = {styles.container}>
                <View style={{marginTop:64, alignItems:'center'}}>
                    <View style = {styles.avatarcontainer}>
                        <Image style={styles.avatar} source = {require('./assets/logo2.png')} />
                    </View>
                    <Text style={styles.name}>Mohamad facahyariskandar</Text>
                </View>
                <View style={styles.statsContainer}>
                    <View style={styles.stat}>
                        <Text style={styles.statAmount}>89</Text>
                        <Text style={styles.statTitle}>Post</Text>
                    </View>
                    <View style={styles.stat}>
                        <Text style={styles.statAmount}>990</Text>
                        <Text style={styles.statTitle}>Followers</Text>
                    </View>
                    <View style={styles.stat}>
                        <Text style={styles.statAmount}>10</Text>
                        <Text style={styles.statTitle}>Following</Text>
                    </View>
                </View>
                <View style={styles.status}>
                    <Text style={styles.study}>Program study : </Text>
                        <Text style ={styles.state}>Teknik Informatika</Text>
                </View>
                <View style={styles.class}>
                    <Text style={styles.kelas}>kelas : </Text>
                    <Text style ={styles.state}>Malam B</Text>
                </View>
                <View style={styles.instagram}>
                    <Text style={styles.ig}>Instagram :</Text>
                    <Text style ={styles.state}>@fariss24</Text>
                </View>
                <View style={styles.facebook}>
                    <Text style={styles.fb}>Facebook :</Text>
                    <Text style ={styles.state}>faris</Text>
                </View>
            </View>
        )
    }
} 
const styles = StyleSheet.create({
    container:{
        backgroundColor: 'grey',
        flex:1,
        width: 400
    },
    avatarcontainer: {
        shadowColor:'#151734',
        shadowRadius: 15,
        shadowOpacity: 0.4
    },
    avatar: {
        width: 200,
        height: 120,
        borderRadius: 68
    },
    name: {
        color: 'black',
        marginTop: 24,
        fontSize: 20,
        fontWeight: 'bold'
    },
    statsContainer: {
        flexDirection: "row",
        justifyContent: 'space-between',
        margin: 20,
        bottom: 20
    },
    stat: {
        alignItems: 'center',
        flex : 1
    },
    statAmount:{
        color:'#4F566D',
        fontSize: 18,
        fontWeight: 'bold'
    },
    statTitle: {
        fontSize: 16,
        color: 'black',
        fontWeight: '300',
        marginTop: 4
    },
    status: {
        bottom: 30,
        left: 30
    },
    study:{
        justifyContent: 'flex-end',
        color: 'black',
        fontSize: 30,
        fontWeight: 'bold',
    },
    class: {
        bottom:14,
        left: 30
    },
    kelas:{
        fontWeight: 'bold',
        fontSize : 30
    },
    instagram: {
        bottom :-14,
        left : 30
    },
    ig: {
        fontSize: 30,
        fontWeight: 'bold'
    },
    facebook:{
        bottom: -40,
        left : 30
    },
    fb: {
        fontWeight :'bold',
        fontSize: 30
    },
    state : {
        fontSize : 25,
        left : 60,
        bottom :-15
    }
});